﻿namespace Prueba.Diego8a.Model
{
    public class Device
    {
        public int Id { get; set; }

        public string  Name { get; set; }

        public string MacAddress { get; set; }

        public string Rssi { get; set; }
    }
}
