﻿using Prism.Commands;
using Prism.Navigation;
//using Plugin.BluetoothLE;

namespace Prueba.Diego8a.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="navigationService"></param>
        public MainPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Prueba Técnica";
            _navigationService = navigationService;
            NavigateToSearchPageCommand = new DelegateCommand(NavigateToSearchPage);
        }

        private INavigationService _navigationService;

        public DelegateCommand NavigateToSearchPageCommand { get; private set; }

        private void NavigateToSearchPage()
        {
            _navigationService.NavigateAsync("InicioPage");
        }
    }
}
