﻿using Plugin.BLE;
using Plugin.BLE.Abstractions.Exceptions;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Prueba.Diego8a.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;

namespace Prueba.Diego8a.ViewModels
{
    public class InicioPageViewModel : ViewModelBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="navigationService"></param>
        /// <param name="dialogService"></param>
        public InicioPageViewModel(INavigationService navigationService, IPageDialogService dialogService)
            : base(navigationService)
        {
            Title = "Buscar Bluetooth LE";
            SearchBluetoothCommand = new DelegateCommand(SearchBluetooth);
            DialogService = dialogService;
        }
        #region Propiedades

        private Device _selectedItem;
        private Boolean _isVisible;
        private Boolean _isRunningIndicator;
        private IPageDialogService DialogService;
        public DelegateCommand SearchBluetoothCommand { get; private set; }
        private ObservableCollection<Device> _deviceList = new ObservableCollection<Device>();

        private static IList<Plugin.BLE.Abstractions.Contracts.IDevice> ListDevices =
           new List<Plugin.BLE.Abstractions.Contracts.IDevice>();

        public Boolean IsVisible
        {
            get { return _isVisible; }
            set { SetProperty(ref _isVisible, value); }
        }

        public Device SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (value != null)
                {
                    ValidateConnectAsync(value);
                    SetProperty(ref _selectedItem, value);
                }
            }
        }

        public Boolean IsRunningIndicator
        {
            get { return _isRunningIndicator; }
            set { SetProperty(ref _isRunningIndicator, value); }
        }


        public ObservableCollection<Device> DeviceList
        {
            get { return _deviceList; }
            set { SetProperty(ref _deviceList, value); }
        }


        #endregion

        #region Metodos
        /// <summary>
        /// Metodo que busca los dispositivos
        /// </summary>
        /// <param name="adapter"></param>
        private void BuscarDispositivos(Plugin.BLE.Abstractions.Contracts.IAdapter adapter)
        {
            adapter.DeviceDiscovered += (s, e) =>
            {
                if (DeviceList == null)
                {
                    DeviceList = new ObservableCollection<Device>();
                }
                if (!ListDevices.Contains(e.Device))
                {
                    ListDevices.Add(e.Device);
                    DeviceList.Add(new Device
                    {
                        Name = e.Device.Name != null ? e.Device.Name.ToString() : "Unknown Device",
                        MacAddress = e.Device.NativeDevice != null ? e.Device.NativeDevice.ToString() : "Unknown Device",
                        Rssi = e.Device.Rssi.ToString()
                    });
                    IsVisible = true;
                    Debug.WriteLine($"The Devices Discovered {e.Device.NativeDevice.ToString()}");
                }
            };

            adapter.StartScanningForDevicesAsync();
        }

        public async void ValidateConnectAsync(Device device)
        {
            var action = await this.DialogService.DisplayAlertAsync("Question?", "Desea Conectarse", "Yes", "No");
            if (action)
            {
                try
                {
                    var adapter = CrossBluetoothLE.Current.Adapter;
                    Plugin.BLE.Abstractions.Contracts.IDevice d = ListDevices
                        .Where(x => x.NativeDevice.ToString().Equals(device.MacAddress)).FirstOrDefault();
                    this.IsRunningIndicator = true;
                    await adapter.ConnectToDeviceAsync(d);
                    await this.DialogService.DisplayAlertAsync("Error",
                        "No se ha podido conectar con el dispositivo Bluetooth", "Aceptar");
                    IsRunningIndicator = false;
                }
                catch (DeviceConnectionException e)
                {
                    IsRunningIndicator = false;
                    await this.DialogService.DisplayAlertAsync("Error",
                        "No se ha podido conectar con el dispositivo Bluetooth", "Aceptar");
                }
            }
        }
        #endregion

        #region Delegates
        /// <summary>
        /// Delegado para buscar Dispositivos
        /// </summary>
        private void SearchBluetooth()
        {
            IsVisible = false;
            //se valida que el bluetooth este encendido
            if (CrossBluetoothLE.Current.IsOn)
            {
                var ble = CrossBluetoothLE.Current;
                var adapter = CrossBluetoothLE.Current.Adapter;

                ble.StateChanged += (s, e) =>
                {
                    Debug.WriteLine($"The bluetooth state changed to {e.NewState}");
                };
                //Metodo que busca los dispositivos
                BuscarDispositivos(adapter);
            }
            else
            {
                this.DialogService.DisplayAlertAsync("Advertencia", "El Bluetooth no está encendido", "Ok");
            }

        }

        #endregion
    }
}
